$(document).ready(function() {

	/* WORD handling variables */
	var selected_word  = ""; // contains the word literal formed (ex. HAT)
	var selected_tiles = []; // contains the selected words in order
	var submitted_words = []; // contains the submitted words that were formed

	// defines the game's timer amount
	var seconds = 120.0;

	// handles the modal click of "START GAME"
	$('#btnStartGame').on('click', function() {

		// hides the modal
		$('#modal').slideUp();

		// updates the #timer contents per second
		var countdownTimer = setInterval(function() {

			var minutes = Math.round((seconds - 30)/60);
		    var remainingSeconds = seconds % 60;

		    if (remainingSeconds < 10)
		        remainingSeconds = "0" + remainingSeconds;  
		    
		    document.getElementById('timer').innerHTML = minutes + ":" + remainingSeconds;
		    console.log(minutes + ":" + remainingSeconds);

		    if (seconds == 0) {
		        clearInterval(countdownTimer);
		        document.getElementById('timer').innerHTML = "Times up!";
		    }
		    else
		        seconds--;

	    }, 1000);
	});


	// handles the tile click event
	$('.tile-wrapper').on('click', function() {

		// displays the (x,y) position of the clicked tile
		var tile = $(this).attr('id');

		// gets the LETTER of the clicked tile
		var letter = $(this).find('span.letter').text();

		// if word is not selected and is the move is allowed
		if (!check_if_selected(tile) && allowed_move(tile)) {

			// mark newly selected tile as BLUE and its text WHITE
			$("div#" + tile + " .outer-wrap").css('background-color', '#2a80b9');
			$("div#" + tile + " .inner-wrap").css('background-color', '#3598db');
			$("div#" + tile + " .letter").css('color', 'white');
			
			// recolor previous last selected work as RED
			$("div#" + selected_tiles[selected_tiles.length-1] + " .outer-wrap").css('background-color', '#c1392b');
			$("div#" + selected_tiles[selected_tiles.length-1] + " .inner-wrap").css('background-color', '#e84c3d');

			selected_word += letter;
			selected_tiles.push(tile);

			// updates the span element that spells out the word
			$("#selected-word").html(selected_word);
		}

		// if word is deselected by pressing the last selected tile
		else if (check_if_selected(tile) && 
				 tile === selected_tiles[selected_tiles.length - 1]) {

			// since it's deselected, color back to WHITE and text to BLACK
			$("div#" + tile + " .outer-wrap").css('background-color', '#bec3c7');
			$("div#" + tile+ " .inner-wrap").css('background-color', 'white');
			$("div#" + tile + " .letter").css('color', 'rgba(0,0,0,0.7');

			selected_word = selected_word.substring(0, selected_tiles.length - 1);
			selected_tiles.pop();

			// recolor new last selected work as BLUE
			$("div#" + selected_tiles[selected_tiles.length-1] + " .outer-wrap").css('background-color', '#2a80b9');
			$("div#" + selected_tiles[selected_tiles.length-1] + " .inner-wrap").css('background-color', '#3598db');

			// updates the span element that spells out the word
			$("#selected-word").html(selected_word);
		}
	});

	/* handles the `submit` button click that adds the formed word
	*  to the word list.
	*/ 
	$('.button#submit').on('click', function() {

		console.log("click");

		// prevents adding of words less than the allowed length
		if (selected_tiles.length < 3)
			return;

		if (!check_word_if_submitted(selected_word)) {

			// adds the user selected formed word to the word list
			$('#word-list').append("<li>" + selected_word + "</li>");

			// adds the submitted word to the stack
			submitted_words.push(selected_word);
		}

		// reset the word handlers
		selected_word  = "";
		selected_tiles = [];

		// reset all tile colors 
		$(".outer-wrap").css('background-color', '#bec3c7');
		$(".inner-wrap").css('background-color', 'white');
		$(".letter").css('color', 'rgba(0,0,0,0.7');

		// reset selected word indicator
		$("#selected-word").html("");
	});	

	/* Checks if the given tile is already selected. 
	*  This is done by check the queue for the tile.
	*/
	function check_if_selected(tile) {
		for (var i = 0; i < selected_tiles.length; i++)
			if (selected_tiles[i] === tile)
				return true;
		return false;
	}

	/* Checks if the selected tile is adjacent to the
	*  last tile selected. Thus, it essentially checks
	*  if it follows the game rules.
	*/
	function allowed_move(tile) {

		// if no tile is selected yet, allow move
		if (selected_tiles.length === 0 )
			return true;

		// contains the x-y coordinate of the newly selected tile
		var x_and_y = tile.split("-");

		// retrieves the previously selected tile
		var last_tile_selected = selected_tiles[selected_tiles.length - 1];

		var prev = last_tile_selected.split("-");

		// checks adjacency of newly selected tile to the previous one
		if ( Math.abs(x_and_y[0] - prev[0] ) <= 1 &&
			 Math.abs(x_and_y[1] - prev[1] ) <= 1) 
			return true;
		else
			return false;
	}

	/* Checks if the word is already submitted and was
	*  added to the word-list. This is done by check the 
	*  `submitted_words` stack
	*/
	function check_word_if_submitted(word) {

		for (var i = 0; i < submitted_words.length; i++)
			if (submitted_words[i] === word)
				return true;
		return false;
	}

});































